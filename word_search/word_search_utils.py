from config import Dim


def read_wordsearch_file(input_file_path):
    with open(input_file_path) as f:
        # get the grid dimensions line
        dimension_line = f.readline().strip().split("x")
        dimensions_as_numbers = [eval(i) for i in dimension_line]
        dimensions = Dim(dimensions_as_numbers[0], dimensions_as_numbers[1])

        # get the grid
        grid_row_count = dimensions.rows
        grid = []

        for x in range(grid_row_count):
            grid_row = f.readline().strip()
            grid_row_as_list = grid_row.split(" ")
            grid.append(grid_row_as_list)

        # get the answer key
        # remove spaces and new lines
        answer_line = f.readline().strip().replace(' ', '')

        words = []
        while answer_line:
            words.append(answer_line)
            answer_line = f.readline().strip().replace(' ', '')

        return dimensions, grid, words
