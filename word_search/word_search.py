import word_search_utils
from config import Coord, POSSIBLE_DIRECTIONS, get_cl_arg


def do_word_search(input_file_path):
    dimensions, grid, words = word_search_utils.read_wordsearch_file(input_file_path)

    results = []
    for word in words:
        start_coord, end_coord = find_word(word=word, grid=grid, dimensions=dimensions)
        if start_coord is not None:
            s = "{} {}:{} {}:{}".format(word, start_coord.x, start_coord.y, end_coord.x, end_coord.y)
            results.append(s)
            print(s)
    return results


def find_word(word, grid, dimensions):
    # turn word into a list of characters
    word_letters = [*word]

    for x in range(dimensions.rows):
        for y in range(dimensions.cols):
            # check for the first letter
            if grid[x][y] == word_letters[0]:
                if len(word_letters) == 1:
                    return Coord(x, y), Coord(x, y)
                # lop off the first letter
                new_word = word_letters[1:]
                start_coord = Coord(x, y)
                result = check_all_directions(grid=grid, start_coord=start_coord, word=new_word, dimensions=dimensions)
                if result is not None:
                    return start_coord, Coord(result[0], result[1])
    return None, None


def check_all_directions(grid, start_coord, word, dimensions):
    # assume we have the first letter at start_coord
    for direction in POSSIBLE_DIRECTIONS:
        val = check_one_direction(grid=grid, word=word, dimensions=dimensions, start_coord=start_coord,
                                  direction=direction)
        if val is not None:
            return val
    return None


def check_one_direction(grid, word, dimensions, start_coord, direction):
    final_coord = None
    new_coord = start_coord

    for letter in word:
        if can_go_direction(dimensions=dimensions, coord=new_coord, direction=direction):
            new_coord = Coord(new_coord.x + direction[0], new_coord.y + direction[1])
            if letter is not grid[new_coord.x][new_coord.y]:
                return None
            else:
                final_coord = new_coord
        else:
            return None
    return final_coord


def can_go_direction(dimensions, coord, direction):
    if coord.x + direction[0] not in range(0, dimensions.rows):
        return False
    return coord.y + direction[1] in range(0, dimensions.cols)


if __name__ == '__main__':
    do_word_search(input_file_path=get_cl_arg())
