from collections import namedtuple
import argparse
import os

DEFAULT_INPUT_PATH = "files/input_6_x_7.txt"
POSSIBLE_DIRECTIONS = [[-1, 1], [0, 1], [1, 1], [-1, 0], [1, 0], [-1, -1], [0, -1], [1, -1]]
Coord = namedtuple("Coord", "x y")
Dim = namedtuple("Dim", "rows cols")


def get_cl_arg():
    parser = argparse.ArgumentParser(description='Enlighten Wordsearch challenge')
    parser.add_argument('--input_file', required=False, help='The input file for the WordSearch puzzle')
    args = parser.parse_args()

    if args.input_file is not None and os.path.isfile(args.input_file):
        return args.input_file
    else:
        # use the default file if no argument supplied
        return DEFAULT_INPUT_PATH
