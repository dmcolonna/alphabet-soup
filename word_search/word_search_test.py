import pytest

import word_search
from config import Coord, Dim


def test_word_search():
    results = word_search.do_word_search(input_file_path="files/input_6_x_7.txt")
    assert len(results) == 6
    assert results[0] == "GOOD 5:0 5:3"
    assert results[1] == "BYE 2:3 2:1"
    assert results[2] == "NEEDY 1:5 5:5"
    assert results[3] == "THINGY 0:0 0:5"
    assert results[4] == "TOASTY 0:6 5:6"
    assert results[5] == "Z 3:3 3:3"


can_go_test_data = [
    (0, 0, [-1, -1], False),
    (0, 0, [1, 1], True),
    (0, 0, [1, -1], False),
    (0, 0, [-1, 1], False),
    (0, 0, [1, 0], True),
    (0, 0, [0, 1], True),
    (2, 2, [-1, -1], True),
    (2, 2, [1, 1], False),
    (2, 2, [1, -1], False),
    (2, 2, [-1, 1], False),
    (2, 2, [1, 0], False),
    (2, 2, [0, 1], False),
]


@pytest.mark.parametrize("x, y, direction, expected", can_go_test_data)
def test_can_go_dimensions(x, y, direction, expected):
    dimensions = Dim(3, 3)
    assert expected == word_search.can_go_direction(dimensions=dimensions, coord=Coord(x, y), direction=direction)
